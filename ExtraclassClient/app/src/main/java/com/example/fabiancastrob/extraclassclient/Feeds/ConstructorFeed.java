package com.example.fabiancastrob.extraclassclient.Feeds;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fabiancastrob.extraclassclient.MessageViewer.MessageViewer;
import com.example.fabiancastrob.extraclassclient.Objects.Mensaje;
import com.example.fabiancastrob.extraclassclient.R;

import java.util.LinkedList;

public class ConstructorFeed {
    LinearLayout linearLayout;
    Context context;
    LinkedList<Mensaje> listaMensajes;

    public ConstructorFeed(LinearLayout linearLayout, Context context, String titulo) {
        this.linearLayout = linearLayout;
        this.context = context;
        this.listaMensajes = new LinkedList<>();

        this.inicializar(titulo);
    }

    private void inicializar(String titulo){


        RelativeLayout relativeLayout = new RelativeLayout(context);
        TextView mensajesRecibidos = new TextView(context);
        mensajesRecibidos.setText(titulo);
        mensajesRecibidos.setTextColor(context.getResources().getColor(R.color.negro));
        mensajesRecibidos.setTypeface(null, Typeface.BOLD);
        mensajesRecibidos.setId(5);
        mensajesRecibidos.setTextSize(16);

        RelativeLayout.LayoutParams mensajesRecibidosLp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        mensajesRecibidosLp.addRule(RelativeLayout.CENTER_HORIZONTAL);
        mensajesRecibidosLp.setMargins(10, 50, 10, 0);

        relativeLayout.addView(mensajesRecibidos, mensajesRecibidosLp);
        this.agregarSeparador(relativeLayout, mensajesRecibidos);

        linearLayout.addView(relativeLayout);
    }

    public void agregarNuevoMensaje(Mensaje mensaje){
        listaMensajes.add(mensaje);
        RelativeLayout relativeLayout = new RelativeLayout(context);
        RelativeLayout.LayoutParams imagenLp = new RelativeLayout.LayoutParams(160, 160);
        imagenLp.setMargins(10, 10, 20, 10);
        ImageView imgView = new ImageView(context);
        imgView.setId(1);

        RelativeLayout.LayoutParams asuntoLp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        TextView asuntoTextView = new TextView(context);
        asuntoTextView.setTextColor(context.getResources().getColor(R.color.negro));
        asuntoTextView.setId(2);
        asuntoTextView.setTypeface(null, Typeface.BOLD_ITALIC);

        if(mensaje.getTipo().equals("mensaje")){
            imgView.setImageResource(R.drawable.mensaje);
            asuntoTextView.setText("Asunto: Mensaje");
        }
        else if(mensaje.getTipo().equals("peticion")){
            imgView.setImageResource(R.drawable.peticion);
            asuntoTextView.setText("Asunto: Petición");
        }
        else{
            imgView.setImageResource(R.drawable.tarea);
            asuntoTextView.setText("Asunto: Tarea");
        }

        RelativeLayout.LayoutParams mensajeLp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        TextView mensajeTextView = new TextView(context);
        mensajeTextView.setText(mensaje.getMensaje());
        mensajeTextView.setTextColor(context.getResources().getColor(R.color.negro));
        mensajeTextView.setMaxLines(4);
        mensajeTextView.setId(3);

        imagenLp.addRule(RelativeLayout.ALIGN_RIGHT);
        relativeLayout.addView(imgView, imagenLp);

        relativeLayout.addView(asuntoTextView, asuntoLp);
        asuntoLp.addRule(RelativeLayout.RIGHT_OF, imgView.getId());

        relativeLayout.addView(mensajeTextView, mensajeLp);
        mensajeLp.addRule(RelativeLayout.RIGHT_OF, imgView.getId());
        mensajeLp.addRule(RelativeLayout.BELOW, asuntoTextView.getId());

        continuarAgregarnuevoMensaje(relativeLayout, imgView, mensaje);
    }



    private void continuarAgregarnuevoMensaje(RelativeLayout relativeLayout, View view, final Mensaje mensaje){
        relativeLayout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                //Toast.makeText(context, "Prueba", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(context, MessageViewer.class);
                i.putExtra("mensaje", mensaje);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        });
        agregarSeparador(relativeLayout, view);
        linearLayout.addView(relativeLayout);
    }

    private void agregarSeparador(RelativeLayout relativeLayout, View view) {
        TextView separadorTextView = new TextView(context);
        separadorTextView.setText("_____________________________________");
        separadorTextView.setTextColor(context.getResources().getColor(R.color.negro));
        separadorTextView.setTypeface(null, Typeface.BOLD_ITALIC);

        RelativeLayout.LayoutParams separadorLp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        separadorLp.setMargins(0, 10, 0, 30);

        separadorLp.addRule(RelativeLayout.BELOW, view.getId());
        separadorLp.addRule(RelativeLayout.CENTER_HORIZONTAL);
        relativeLayout.addView(separadorTextView, separadorLp);
    }
}
