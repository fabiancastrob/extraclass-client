package com.example.fabiancastrob.extraclassclient.MessageViewer;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fabiancastrob.extraclassclient.Objects.Mensaje;
import com.example.fabiancastrob.extraclassclient.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MessageViewer extends AppCompatActivity {
    ViewPager viewPager;
    TextView txtGrupo;
    TextView txtDocente;
    TextView txtEscuela;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_viewer);

        viewPager = (ViewPager) findViewById(R.id.viewPager);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(this);

        viewPager.setAdapter(viewPagerAdapter);

        Intent msj = getIntent();
        Mensaje mensaje = (Mensaje) msj.getSerializableExtra("mensaje");

        TextView asunto = (TextView) findViewById(R.id.txtAsunto);
        asunto.setText(asunto.getText() + " " + mensaje.getTipo());

        TextView mensj = (TextView) findViewById(R.id.txtMensaje);
        mensj.setText(mensj.getText() + " " + mensaje.getMensaje());

        txtGrupo = (TextView) findViewById(R.id.txtGrupo);
        txtEscuela = (TextView) findViewById(R.id.txtEscuela);
        txtDocente = (TextView) findViewById(R.id.txtEnviadoPor);
        this.obtenerDatos(mensaje.getIdGrupo(), mensaje.getIdEscuela(), mensaje.getIdDocente());
    }

    private void obtenerDatos(final String idGrupo, final String idEscuela, final String idDocente){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference();

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot datSnapshot: dataSnapshot.child("escuelas").getChildren()) {
                    if(datSnapshot.child("id").getValue().equals(idEscuela)){
                        txtEscuela.setText("Escuela: " + datSnapshot.child("nombre").getValue());
                        for(DataSnapshot dSnapshot : datSnapshot.child("grupos").getChildren()){
                            if(dSnapshot.child("id").getValue().equals(idGrupo)){
                                txtGrupo.setText("Grupo: " + (String) dSnapshot.child("descripcion").getValue());
                            }
                        }
                    }
                }

                for(DataSnapshot datasSnapshot : dataSnapshot.child("docentes").getChildren()){
                    if(datasSnapshot.child("id").getValue().equals(idDocente))
                        txtDocente.setText("Enviado por: " + datasSnapshot.child("nombre").getValue());
                }
            }

            @Override
            public void onCancelled(DatabaseError firebaseError) {
                firebaseError.toException();
            }
        });
    }


    public void volver(View view){
        this.finish();
    }




}
