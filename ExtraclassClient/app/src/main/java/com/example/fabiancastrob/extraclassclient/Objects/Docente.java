package com.example.fabiancastrob.extraclassclient.Objects;

import java.util.LinkedList;

public class Docente {
    String id;
    String nombre;
    LinkedList<Escuela> escuelas;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public LinkedList<Escuela> getEscuelas() {
        return escuelas;
    }

    public void setEscuelas(LinkedList<Escuela> escuelas) {
        this.escuelas = escuelas;
    }

    public Docente(String id, String nombre, LinkedList<Escuela> escuelas) {

        this.id = id;
        this.nombre = nombre;
        this.escuelas = escuelas;
    }
}
