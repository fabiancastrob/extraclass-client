package com.example.fabiancastrob.extraclassclient.Objects;

import java.util.LinkedList;

public class Escuela {
    String id;
    String nombre;
    LinkedList<Grupo> grupos;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public LinkedList<Grupo> getGrupos() {
        return grupos;
    }

    public void setGrupos(LinkedList<Grupo> grupos) {
        this.grupos = grupos;
    }

    public Escuela(String id, String nombre, LinkedList<Grupo> grupos) {

        this.id = id;
        this.nombre = nombre;
        this.grupos = grupos;
    }
}
