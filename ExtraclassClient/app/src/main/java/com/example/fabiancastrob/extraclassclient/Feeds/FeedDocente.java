package com.example.fabiancastrob.extraclassclient.Feeds;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.fabiancastrob.extraclassclient.Objects.Mensaje;
import com.example.fabiancastrob.extraclassclient.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.LinkedList;

public class FeedDocente extends AppCompatActivity {
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_docente);

        linearLayout = findViewById(R.id.layoutFeedClient);
        final TextView txtUsuario = findViewById(R.id.txtUsuario);
        final int idDocente = 0;

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference();

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = (String) dataSnapshot.child("docentes").child(String.valueOf(idDocente)).child("nombre").getValue();
                txtUsuario.setText("Usuario: " + value + " (Docente)");
            }

            @Override
            public void onCancelled(DatabaseError firebaseError) {
                firebaseError.toException();
            }
        });

        final ConstructorFeed constructorFeed = new ConstructorFeed(linearLayout, this, "MENSAJES ENVIADOS");
        ref.child("mensajes").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //todo aquí se debe hacer la validación de los mensajes que estarían relacionados al encargado en específico
                for (DataSnapshot datSnapshot: dataSnapshot.getChildren()) {
                    if(Integer.parseInt(datSnapshot.child("idDocente").getValue().toString()) == idDocente){
                        constructorFeed.agregarNuevoMensaje(datSnapshot.getValue(Mensaje.class));
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError firebaseError) {
                firebaseError.toException();
            }
        });
    }
}
