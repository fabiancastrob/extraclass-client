package com.example.fabiancastrob.extraclassclient.Objects;

import java.util.LinkedList;

public class Encargado {
    String id;
    String correo;
    String nombre;
    LinkedList<Grupo> grupos;

    public Encargado(String id, String correo, String nombre, LinkedList<Grupo> grupos) {
        this.id = id;
        this.correo = correo;
        this.nombre = nombre;
        this.grupos = grupos;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public LinkedList<Grupo> getGrupos() {
        return grupos;
    }

    public void setGrupos(LinkedList<Grupo> grupos) {
        this.grupos = grupos;
    }
}
