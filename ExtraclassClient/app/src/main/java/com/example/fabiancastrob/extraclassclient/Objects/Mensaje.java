package com.example.fabiancastrob.extraclassclient.Objects;

import java.io.Serializable;

public class Mensaje implements Serializable {
    String id;
    String idEscuela;
    String idGrupo;
    String tipo;
    String mensaje;
    String idDocente;

    public Mensaje(String id, String idEscuela, String idGrupo, String tipo, String mensaje, String idDocente) {
        this.id = id;
        this.idEscuela = idEscuela;
        this.idGrupo = idGrupo;
        this.tipo = tipo;
        this.mensaje = mensaje;
        this.idDocente = idDocente;
    }

    public Mensaje() {
    }

    public String getIdDocente() {
        return idDocente;
    }

    public void setIdDocente(String idDocente) {
        this.idDocente = idDocente;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdEscuela() {
        return idEscuela;
    }

    public void setIdEscuela(String idEscuela) {
        this.idEscuela = idEscuela;
    }

    public String getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(String idGrupo) {
        this.idGrupo = idGrupo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
