package com.example.fabiancastrob.extraclassclient.Login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.fabiancastrob.extraclassclient.Feeds.FeedDocente;
import com.example.fabiancastrob.extraclassclient.Feeds.FeedEncargado;
import com.example.fabiancastrob.extraclassclient.R;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        //ConstraintLayout loginLayout = (ConstraintLayout) findViewById(R.id.lgn_layout);


        //Para implementar una animación tal vez más adelante
        /*AnimationDrawable animationDrawable = (AnimationDrawable) loginLayout.getBackground();

        animationDrawable.setEnterFadeDuration(5000);
        animationDrawable.setExitFadeDuration(2000);

        animationDrawable.start();*/


    }

    public void ingresarComoEncargado(View view) {
        Intent intent = new Intent(this, FeedEncargado.class);
        startActivity(intent);
    }

    public void ingresarComoDocente(View view){
        Intent intent = new Intent(this, FeedDocente.class);
        startActivity(intent);
    }
}
