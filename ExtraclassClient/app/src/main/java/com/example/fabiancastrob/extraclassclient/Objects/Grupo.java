package com.example.fabiancastrob.extraclassclient.Objects;

import java.util.LinkedList;

public class Grupo {
    String id;
    String idDocente;
    String descripcion;
    LinkedList<Estudiante> estudiantes;

    public Grupo(String id, String idDocente, String descripcion, LinkedList<Estudiante> estudiantes) {
        this.id = id;
        this.idDocente = idDocente;
        this.descripcion = descripcion;
        this.estudiantes = estudiantes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdDocente() {
        return idDocente;
    }

    public void setIdDocente(String idDocente) {
        this.idDocente = idDocente;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public LinkedList<Estudiante> getEstudiantes() {
        return estudiantes;
    }

    public void setEstudiantes(LinkedList<Estudiante> estudiantes) {
        this.estudiantes = estudiantes;
    }
}
